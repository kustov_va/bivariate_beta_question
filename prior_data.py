from datetime import datetime

import numpy as np
import pandas as pd


n = 100

exp_a = 'exp_a'
trials_a = np.random.normal(4000, 300, n).astype('int')
successes_a = np.random.binomial(trials_a, 0.005)
end_a = datetime(2019, 10, 1)
dates_a = pd.date_range(end=end_a, periods=n, freq='D')
df_a = pd.DataFrame({'exp_id': exp_a, 'date': dates_a, 'trials': trials_a, 'successes': successes_a})

exp_b = 'exp_b'
trials_b = np.random.normal(1000, 100, n).astype('int')
successes_b = np.random.binomial(trials_b, 0.02)
end_b = datetime(2019, 11, 1)
dates_b = pd.date_range(end=end_b, periods=n, freq='D')
df_b = pd.DataFrame({'exp_id': exp_b, 'date': dates_b, 'trials': trials_b, 'successes': successes_b})

df = pd.concat([df_a, df_b])

if __name__ == '__main__':
    pass
