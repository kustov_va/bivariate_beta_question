from datetime import datetime

import numpy as np
import pandas as pd


n_a = 28
exp_a = 'exp_a'
trials_a_0 = np.random.normal(2000, 300, n_a).astype('int')
trials_a_1 = np.random.normal(2000, 300, n_a).astype('int')
successes_a_0 = np.random.binomial(trials_a_0, 0.005)
successes_a_1 = np.random.binomial(trials_a_1, 0.005)
start_a = datetime(2019, 10, 2)
index_a = pd.MultiIndex.from_tuples(
    zip(pd.date_range(start=start_a, periods=n_a, freq='D'), [exp_a]*n_a),
    names=['date', 'exp_id']
)
cols_a = pd.MultiIndex.from_tuples(
    [('trials', 0), ('successes', 0), ('trials', 1), ('successes', 1)],
    names=['', 'exp_var']
)
df_a = pd.DataFrame(
    np.hstack((
        trials_a_0.reshape(n_a, 1),
        successes_a_0.reshape(n_a, 1),
        trials_a_1.reshape(n_a, 1),
        successes_a_1.reshape(n_a, 1)
    )), index=index_a, columns=cols_a)
df_a = df_a.stack(level='exp_var').reset_index(drop=False)

n_b = 48
exp_b = 'exp_b'
trials_b_0 = np.random.normal(333, 33, n_b).astype('int')
trials_b_1 = np.random.normal(333, 33, n_b).astype('int')
trials_b_2 = np.random.normal(333, 33, n_b).astype('int')
successes_b_0 = np.random.binomial(trials_b_0, 0.01)
successes_b_1 = np.random.binomial(trials_b_1, 0.02)
successes_b_2 = np.random.binomial(trials_b_2, 0.03)
start_b = datetime(2019, 11, 1)
index_b = pd.MultiIndex.from_tuples(
    zip(pd.date_range(start=start_b, periods=n_b, freq='D'), [exp_b]*n_b),
    names=['date', 'exp_id']
)
cols_b = pd.MultiIndex.from_tuples(
    [('trials', 0), ('successes', 0), ('trials', 1), ('successes', 1), ('trials', 2), ('successes', 2)],
    names=['', 'exp_var']
)
df_b = pd.DataFrame(
    np.hstack((
        trials_b_0.reshape(n_b, 1),
        successes_b_0.reshape(n_b, 1),
        trials_b_1.reshape(n_b, 1),
        successes_b_1.reshape(n_b, 1),
        trials_b_2.reshape(n_b, 1),
        successes_b_2.reshape(n_b, 1)
    )), index=index_b, columns=cols_b)
df_b = df_b.stack(level='exp_var').reset_index(drop=False)

df = pd.concat([df_a, df_b])

if __name__ == '__main__':
    pass
