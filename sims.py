import pandas as pd

dataset = pd.read_csv('data_for_sims.csv', sep='\t', decimal=',')


# 'dataset' содержит входные данные для этого сценария

import pandas as pd
import numpy as np
from scipy.stats import beta

# N_SIMS = 100000
#
#
dataset['date'] = pd.to_datetime(dataset['date'], format='%d.%m.%Y')
# dataset_latest = dataset.\
#     sort_values(by='date', ascending=False).\
#     groupby(by=['exp_id', 'exp_var'], as_index=False).\
#     first()
#
#
# def eval_stats(exp: pd.DataFrame) -> pd.DataFrame:
#     exp_vars = []
#     sims = []
#     for _, row in exp.iterrows():
#         exp_vars.append(row['exp_var'])
#         rv = beta(row['Пост.alpha'], row['Пост.beta'])
#         sims.append(rv.rvs(100000))
#     sims = np.array(sims)
#     winners = pd.Series(np.argmax(sims, axis=0)).value_counts()
#     winners.name = 'won_times'
#     results = pd.DataFrame({'exp_var': exp_vars})
#     results = pd.merge(left=results, right=winners, left_on='exp_var', right_index=True, how='outer')
#     return results
#
#
# def eval_stats(exp: pd.DataFrame) -> pd.DataFrame:
#     sims = dict()
#     for _, row in exp.iterrows():
#         rv = beta(row['Пост.alpha'], row['Пост.beta'])
#         sims[row['exp_var']] = (rv.rvs(N_SIMS))
#     sims = pd.DataFrame(sims)
#
#     # prob to beat all
#     sims = sims.reindex(sorted(sims.columns), axis=1)
#     beat_all = pd.Series(np.argmax(sims.values, axis=1)).value_counts() / N_SIMS
#     beat_all.name = 'prob_to_beat_all'
#
#     # prob to beat var 0 (if it's not the same as to beat all)
#     if len(sims.columns) > 2:
#         beat_0 = dict()
#         for c in sims.columns:
#             if c == 0:
#                 beat_0[c] = 0
#             else:
#                 beat_0[c] = pd.Series(sims[c] > sims[0]).mean()
#         beat_0 = pd.Series(beat_0)
#     else:  # if it's the same as to beat all
#         beat_0 = beat_all.copy()
#         beat_0.loc[0] = 0
#     beat_0.name = 'prob_to_beat_0'
#     res = pd.merge(
#         left=beat_all,
#         right=beat_0,
#         how='outer',
#         left_index=True,
#         right_index=True
#     )
#     res['prob_to_beat_all'].fillna(0, inplace=True)
#     res.index.name = 'exp_var'
#     res.reset_index(drop=False, inplace=True)
#     return res
#
#
# result = dataset_latest.groupby('exp_id', as_index=True).apply(eval_stats)
# result.reset_index(drop=False, level='exp_id', inplace=True)
# result.reset_index(drop=True, inplace=True)


def visualize():
    import matplotlib.pyplot as plt

    def plot_variant(exp_id: str, exp_var: int, cmap_name: str):
        b_2 = dataset[(dataset['exp_id'] == exp_id) & (dataset['exp_var'] == exp_var)]
        b_2.reset_index(drop=True, inplace=True)
        weekdays = b_2['date'].dt.weekday
        b_2 = b_2[weekdays == weekdays.loc[6]]
        b_2.reset_index(drop=True, inplace=True)
        n_lines = 5
        b_2_short = b_2.loc[:n_lines-2]
        # colors
        cmap = plt.get_cmap(cmap_name)
        colors = [cmap(i) for i in np.linspace(0, 1, n_lines)]

        # prior
        rv = beta(b_2.loc[0, 'Приор.alpha'], b_2.loc[0, 'Приор.beta'])
        x = np.linspace(rv.ppf(0.005), rv.ppf(0.995), 100)
        ax.plot(x, rv.pdf(x), label=f'Prior', color=colors[0])
        # weeks
        i = 1
        for week, row in b_2_short.iterrows():
            rv = beta(row['Пост.alpha'], row['Пост.beta'])
            x = np.linspace(rv.ppf(0.005), rv.ppf(0.995), 100)
            ax.plot(x, rv.pdf(x), label=f'Week {week+1}', color=colors[i])
            i += 1

    fig, ax = plt.subplots()
    plot_variant('exp_b', 1, 'autumn')
    plot_variant('exp_b', 2, 'cool')
    ax.legend()
    ax.set_xlabel('CR')
    ax.set_ylabel('f(CR)')
    ax.set_title('Progress of posterior distribution of CR')
    plt.show()

    pass


if __name__ == '__main__':
    visualize()
