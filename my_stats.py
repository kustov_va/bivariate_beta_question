from typing import Tuple

from scipy import stats
import numpy as np
import pandas as pd


DAYS = 30

TRIALS_MEAN = 5000
TRIALS_STD = 200**2
TRIALS_COV_FACTOR = 0.999

PRIOR_ALPHA = 20
PRIOR_BETA = TRIALS_MEAN - PRIOR_ALPHA

PROB_A_ALPHA = PRIOR_ALPHA - 1
PROB_A_BETA = TRIALS_MEAN - PROB_A_ALPHA
PROB_B_ALPHA = PRIOR_ALPHA + 1
PROB_B_BETA = TRIALS_MEAN - PROB_B_ALPHA
BETAS_COPULA_COV_FACTOR = 0.5

np.random.seed(0)


def make_data():
    def make_trials():
        """Creates strongly correlated daily number of trials"""
        mvnorm = stats.multivariate_normal(
            mean=[TRIALS_MEAN, TRIALS_MEAN],
            cov=[[TRIALS_STD, TRIALS_COV_FACTOR*TRIALS_STD],
                 [TRIALS_COV_FACTOR*TRIALS_STD, TRIALS_STD]])
        return mvnorm.rvs(DAYS).astype(int)

    def make_probs():
        """Creates correlated daily ground truth probs of success using normal copula"""

        def covariate(rv1: stats.rv_continuous, rv2: stats.rv_continuous, cov_factor: float, n: int):
            def make_copula():
                mvnorm = stats.multivariate_normal(mean=[0, 0], cov=[[1., cov_factor],
                                                                     [cov_factor, 1.]])
                # Generate random samples from multivariate normal with covariance
                x = mvnorm.rvs(n)
                return stats.norm.cdf(x=x)

            copula = make_copula()
            s1 = rv1.ppf(copula[:, 0])
            s2 = rv2.ppf(copula[:, 1])
            return np.vstack((s1, s2))

        beta_a = stats.beta(PROB_A_ALPHA, PROB_A_BETA)
        beta_b = stats.beta(PROB_B_ALPHA, PROB_B_BETA)
        return covariate(rv1=beta_a, rv2=beta_b, cov_factor=BETAS_COPULA_COV_FACTOR, n=DAYS).T

    trials = make_trials()
    probs = make_probs()
    # creates daily number of successes by sampling from binomial with corresponding number of trials
    # and prob of success

    successes = np.random.binomial(trials, probs)

    df = pd.DataFrame(
        np.hstack(
            (trials, probs, successes)
        ),
        columns=['trials_a', 'trials_b', 'prob_a', 'prob_b', 'successes_a', 'successes_b']
    )
    df['cr_a'] = df['successes_a'] / df['trials_a']
    df['cr_b'] = df['successes_b'] / df['trials_b']
    return df


class BivariateBeta:
    def __init__(self, beta0: Tuple[float, float], beta1: Tuple[float, float], data: np.array):
        """Implements 5-parameter bivariate beta distribution as proposed in [1]

        Parameters
        ----------
        beta0 : Tuple[float, float]
            posterior marginal beta distribution of variant0
        beta1 : Tuple[float, float]
            posterior marginal beta distribution of variant1
        data : np.ndarray
            np.ndarray of daily CRs of both variants with shape (2, n)

        Notes
        -----
        .. [1] Barry C.Arnold, Hon Keung Tony Ng, "Flexible bivariate beta distributions"
               https://doi.org/10.1016/j.jmva.2011.04.001
        """
        # those are not estimated as in [1], but given as arguments
        self._a, self._b = beta0
        self._c, self._d = beta1

        if data.shape[0] != 2:
            raise ValueError(f'Data shape must be (2, n). Shape of data is {data.shape}')
        self._data = data

        self.__solve_quadratic()
        self.__get_gammas()

    def __solve_quadratic(self):
        """Implements Modified maximum likelihood estimation (MMLE)"""
        a, b, c, d = self._a, self._b, self._c, self._d
        n = self._data.shape[1]
        x = self._data[0, :]
        y = self._data[1, :]

        b_ = b*c + a*c + a*d - b - d  # denoted as B in [1]
        c_ = (a-1)*(c-1)*b*d - a*c*(a-1)*(c-1)/n * np.sum((1-x)*(1-y)/(x*y))  # denoted as C in [1]

        # those are denoted as \hat{\alpha}_i in [1]
        self._k5 = max(0, (-b_ + np.sqrt(b_**2 - 4*c_))/2)
        self._k4 = max(0, b - self._k5)
        self._k3 = max(0, d - self._k5)
        self._k2 = max(0, c - self._k4)
        self._k1 = max(0, a - self._k3)

    def __get_gammas(self):
        # those are denoted as U_i in [1]
        self.g1 = stats.gamma(self._k1)
        self.g2 = stats.gamma(self._k2)
        self.g3 = stats.gamma(self._k3)
        self.g4 = stats.gamma(self._k4)
        self.g5 = stats.gamma(self._k5)

    def simulate(self, n: int = 10_000):
        def sample(g: stats.gamma):
            if g.args == (0, ):
                return np.zeros(shape=n)
            else:
                return g.rvs(size=n)
        u1 = sample(self.g1)
        u2 = sample(self.g2)
        u3 = sample(self.g3)
        u4 = sample(self.g4)
        u5 = sample(self.g5)

        x = (u1 + u3)/(u1+u3+u4+u5)
        y = (u2 + u4)/(u2+u3+u4+u5)

        return np.vstack((x, y))

    def plot_simulation(self, sim: np.ndarray = None):
        import matplotlib.pyplot as plt
        x = np.linspace(0, 0.01, 100)

        fig = plt.figure()
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222)
        ax3 = fig.add_subplot(212)

        # x = np.linspace(sim[0, :].min(), sim[0, :].max(), 100)
        # ax1.hist(sim[0, :], bins=20, density=True)
        ax1.hist(self._data[0, :], bins=5, density=True)
        ax1.plot(x, stats.beta.pdf(x, self._a, self._b))

        # ax2.hist(sim[1, :], bins=20, density=True)
        ax2.hist(self._data[1, :], bins=5, density=True)
        ax2.plot(x, stats.beta.pdf(x, self._c, self._d))

        plt.show()


def _sim_bivariate():
    df = pd.read_csv('data_for_sims.csv', sep='\t', decimal=',')
    df = df[df['exp_id'] == 'exp_a']
    df['cr'] = df['successes'] / df['trials']
    df = df[['date', 'exp_var', 'cr', 'Пост.alpha', 'Пост.beta']]
    df = df.groupby(['date', 'exp_var']).mean().unstack('exp_var')
    a = df[('Пост.alpha', 0)].iloc[-1]
    b = df[('Пост.beta', 0)].iloc[-1]
    c = df[('Пост.alpha', 1)].iloc[-1]
    d = df[('Пост.beta', 1)].iloc[-1]
    df = df['cr']
    bbv = BivariateBeta((a, b), (c, d), data=df.values.T)
    sample = bbv.simulate(100000)
    bbv.plot_simulation(sim=sample)
    pass


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    experiment_df = make_data()
    experiment_df[['cr_a', 'cr_b']].plot()
    plt.show()
    pass
